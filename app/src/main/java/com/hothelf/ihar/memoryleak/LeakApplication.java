package com.hothelf.ihar.memoryleak;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by Ihar on 18.06.2017.
 */

public class LeakApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }

        LeakCanary.install(this);
    }

}
