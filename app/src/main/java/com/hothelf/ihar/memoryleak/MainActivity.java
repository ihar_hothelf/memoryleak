package com.hothelf.ihar.memoryleak;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.squareup.leakcanary.LeakCanary;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private final String TAG = "Leaks";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LeakCanary.enableDisplayLeakActivity(this);

        setContentView(R.layout.activity_main);

        final LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TimeUnit.MINUTES.toMillis(5), 100, this);
    }

    @Override
    public void onLocationChanged(final Location pLocation) {
        Log.d(TAG, "onLocationChanged: " + pLocation.getLatitude());
    }

    @Override
    public void onStatusChanged(final String pProvider, final int pStatus, final Bundle pExtras) {
        Log.d(TAG, "onStatusChanged: " + pStatus);
    }

    @Override
    public void onProviderEnabled(final String pProvider) {
        Log.d(TAG, "onProviderEnabled: " + pProvider);
    }

    @Override
    public void onProviderDisabled(final String pProvider) {
        Log.d(TAG, "onProviderDisabled: " + pProvider);
    }
}
